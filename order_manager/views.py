
from django.shortcuts import render, render_to_response, RequestContext
from django.http import HttpResponse
from django.core import serializers

from .models import order_details
from .models import Order

from django.conf import settings

from django.core.context_processors import csrf
from django.views.decorators.csrf import ensure_csrf_cookie

from django.http import JsonResponse

import json

from django.contrib.auth.decorators import login_required

def order(request):
    # if not request.user.is_authenticated():
    #     return JsonResponse({'isUserAuthenticated': False })
    # else:
    if request.method == 'POST':
        
        jsonString = request.POST.get('json')
        pseudo = request.POST.get('pseudo')
        comment = request.POST.get('comment')
        
        jsonOrder = json.loads(jsonString)
        
        new_order = Order()
        new_order.pseudo = pseudo
        new_order.comment = comment

        #new_order.client_user = request.user
        
        new_order.save()
                
        for order_item in jsonOrder:
            tmp = order_details(name=order_item['name'],quantity=order_item['quantity'], order = new_order)
            tmp.save()
            
        order_detail_list = order_details.objects.all()
        order_list = Order.objects.all()
        context = {'order_list': order_list, 'order_detail_list': order_detail_list,}
        return JsonResponse({'isUserAuthenticated': True ,'order_id':new_order.id})
                   
    else:
        order_detail_list = order_details.objects.all()
        order_list = Order.objects.all()
        context = {'order_list': order_list, 'order_detail_list': order_detail_list,}

    #return render(request, 'order_manager/order_details.html', context)
    return render_to_response("order_manager/order_details.html",
                              locals(),
                              context_instance=RequestContext(request)
                              )
@login_required
def changeOrderStatus(request):
    
    from django.utils import timezone
    
    if request.method == 'POST':
        orderId = request.POST.get('id')
        newStatus = request.POST.get('status')
        order = Order.objects.get(id = orderId)
        order.status = newStatus
        
        if (newStatus == Order.READY_STATUS or newStatus == Order.REFUSED_STATUS):
            order.processedTime = timezone.now()
        
        order.save()

    return JsonResponse({'status':'ok'})


#@login_required
def orderStatus(request):
    if request.method == 'POST':
        orderId = request.POST.get('id')
    elif request.method == 'GET':
        orderId = request.GET.get('id')
        
    order = Order.objects.get(id = orderId)

    return JsonResponse({'order_status':order.status})