function resetAllInput() {
    "use strict";
    $("div[name^='item-']").each(function () {$(this).find("input").val("0");});
}

function updateHeaderTotal() {
    "use strict";
    getLocalOrder().then (function(order){
        var currentTotal = order.totalPrice();
        document.getElementById("baseTotalValue").innerHTML = "Total : " + currentTotal.toFixed(2) + " €";
    });
}