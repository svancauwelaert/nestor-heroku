var refreshEnable = true;

function checkRefresh() {
    "use strict";
    setTimeout(function () {
        if (refreshEnable) {
            location.reload(true);
        }
        checkRefresh();
    }, 15000);
}

// Main

checkRefresh();

/*******************/


function blinkScreen(){
    var timer=0;
    // more details on http://jquery.malsup.com/block/#overview
    $.blockUI({css: { 'font-size':'380%'}, message: "Commande en Attente",overlayCSS: { opacity:0.9 } , timeout:500 });
    $('.blockOverlay').click(function(){
        $.unblockUI;
        clearTimeout(timer);
        sessionStorage.localOrderTime = last_time.getTime(); // last order = new order
        }); // allow unblocking with clicking
    timer = setTimeout(blinkScreen, 1000); // duration of black screen
}

function isLocalOrderTimeAvailable() {
  return sessionStorage.localOrderTime !== undefined;
}


