from datetime import timedelta

from django.shortcuts import render_to_response, RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.db.models import Q

from order_manager.models import Order

from django.contrib.auth.decorators import login_required

@login_required
def home(request):
    delta_minutes = 1 #time after which the accepted orders are filtered out
    order_list = Order.objects.exclude(Q(status__in=[Order.READY_STATUS,Order.REFUSED_STATUS]) & Q(processedTime__lte=timezone.now()-timedelta(minutes=delta_minutes))).order_by('requestedTime')
    return render_to_response("bartender/bar_tender_main.html", locals(), context_instance=RequestContext(request))

@login_required
def oldProcessedOrders(request):
    delta_minutes = 1 #time after which the accepted orders are filtered out
    order_list = Order.objects.filter(Q(status__in=[Order.READY_STATUS,Order.REFUSED_STATUS]) & Q(processedTime__lte=timezone.now()-timedelta(minutes=delta_minutes))).order_by('requestedTime')
    return render_to_response("bartender/bar_tender_old-orders.html", locals(), context_instance=RequestContext(request))
