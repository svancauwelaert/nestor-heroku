/*************************Main*********************/
$(document).ready(function(){
    //check if order has been sent and therefore has a status. If not, we hide everything
    
    getCurrentOrderStatus().then(function(status){
        
        if (status === null) {
        //hide all
        document.getElementById("accepted").style.display = 'none';
        document.getElementById("received").style.display = 'none';
        document.getElementById("ready").style.display = 'none';
        document.getElementById("refused").style.display = 'none';
    } else {
        showStatus(status);
    }
        
    });

    setOrderStatusTitleView();

    getLocalOrder().then (function(order){
        pollStatus(order.id);
    });
});

/**************************************************/

function pollStatus(orderId) {
    "use strict";
    setTimeout(function () {
        $.ajax({
            url: "/order_manager/order_status/",
            type: "GET", // http method
            data: {id: orderId},
            success: function (data) {
                setCurrentOrderStatus(data.order_status);
                getCurrentOrderStatus().then(function(status){
                    showStatus(status);
                });
            },
            // handle a non-successful response
            error: function (xhr, errmsg, err) {
                $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: " + errmsg +
                    " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            },
            complete: pollStatus(orderId)
        });
    }, 15000);
}

function showStatus(orderStatus) {
    "use strict";
    
    if (orderStatus === Order.pendingStatus) {  // pending
        document.getElementById("received").style.display = 'block';
        document.getElementById("accepted").style.display = 'none';
        document.getElementById("ready").style.display = 'none';
        document.getElementById("refused").style.display = 'none';
    }
    if (orderStatus === Order.acceptedStatus) {  // accepted
        document.getElementById("accepted").style.display = 'block';
        document.getElementById("received").style.display = 'none';
        document.getElementById("ready").style.display = 'none';
        document.getElementById("refused").style.display = 'none';
    }
    if (orderStatus === Order.refusedStatus) { // refused
        document.getElementById("received").style.display = 'none';
        document.getElementById("accepted").style.display = 'none';
        document.getElementById("ready").style.display = 'none';
        document.getElementById("refused").style.display = 'block';
    }
    if (orderStatus === Order.readyStatus) { // ready
        document.getElementById("accepted").style.display = 'none';
        document.getElementById("received").style.display = 'none';
        document.getElementById("ready").style.display = 'block';
        document.getElementById("refused").style.display = 'none';
    }
}

function setOrderStatusTitleView() {
    getLocalOrder().then (function(order){
        var id = order.id;
        $('#status-title').text("Commande " + id);
    });
}
