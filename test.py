import unittest
from django.test import TestCase

from django.test import Client  #to test http requests
from bar_admin.models import Menu
#from order_manager.models import Order

class DummyTest(unittest.TestCase):
    "Dummy tests"

    def test_basic(self):
        a = 1
        self.assertEqual(1, a)

    def test_basic_2(self):
        a = 1
        assert a == 1
        
        
class ViewTest(unittest.TestCase):
    "Test if views works"

    def test_home_page(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(200, response.status_code)
        
    def test_get_menu(self):
        #create a fake menu
        myMenu = Menu(available = True)
        myMenu.save()
        
        c = Client()
        response = c.get('/menu/1/')
        self.assertEqual(200, response.status_code)
        
    def test_post_order(self):
                
        c = Client()
        response = c.post('/order_manager/',{'pseudo': 'john', 'comment': 'bande de nuls', 'json' : '[{"name":"Coca 20cl.","quantity":1}]'})
        self.assertEqual(200, response.status_code)
        
    def test_client(self):
        #create a fake menu
        myMenu = Menu(available = True)
        myMenu.save()
        
        c = Client()
        response = c.get('/client/')
        self.assertEqual(200, response.status_code)
    
    #todo add a category and test its view
    
    def test_myorder(self):
        c = Client()
        response = c.get('/client/my_order/')
        self.assertEqual(200, response.status_code)