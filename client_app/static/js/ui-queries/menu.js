$( document ).ready(function() {
    //var menuPromise = getLocalMenu();
    getLocalMenu().then (function(localMenu){
        fillCategoryContainer(localMenu);
        getLocalOrder().then (function(localOrder){
            showBadges(localMenu,localOrder);
        });
    });
    
});

function fillCategoryContainer(menu) {
	var categories = menu.categories;
	for (var categoryId in categories) {
		var category = categories[categoryId];
		$('#category-container').append(categoryItemListHTML(category.name,category.id));
	}
}

function categoryItemListHTML(name, id) {

	var html = '<div class="row " style="border: 1px solid #eee; color: inherit;"> \
      		<a style="color: inherit;" href="category/?cat_id=' + id + '"> \
        		<h3 class="col-xs-8 ">' + name + '</h3> \
        		<div class="col-xs-2" style="padding-top: 8px;"> \
          			<div class="badge" id="cat-' + id + '"> 0 </div> \
        		</div> \
        		<h3 class="col-xs-2" style="text-align: right">></h3> \
      		</a> \
    	</div>';

    return html;
}

//TODO : the badges could be managed in a more modular way than this, using functions
function showBadges(menu, order) {
    var categories = menu.categories;
    //getLocalOrder().then (function(order){
        
        for (var categoryId in categories) {
            
            var nbItemsForCategory = order.numberOfItemsInCategory(categoryId);
            if (nbItemsForCategory>0) {
                document.getElementById("cat-"+categoryId).textContent=nbItemsForCategory;
            }
            else document.getElementById("cat-"+categoryId).style.display = 'none';
        }
    //})
 }