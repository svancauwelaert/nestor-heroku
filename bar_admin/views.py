
from rest_framework.response import Response
from rest_framework.decorators import api_view

from bar_admin.models import Menu, Menu_category, menu_item
from bar_admin.serializers import MenuSerializer, CategorySerializer, MenuItemSerializer

@api_view(['GET',])
def menu(request, id, format=None):
    if request.method == 'GET':
        
        my_menu = Menu.objects.get(id=id)
        serializer = MenuSerializer(my_menu)
        return Response(serializer.data)
        
    else:
        return

@api_view(['GET',])
def category(request, id, format=None):
    if request.method == 'GET':
        
        cat = Menu_category.objects.get(id=id)
        serializer = CategorySerializer(cat)
        return Response(serializer.data)
    

@api_view(['GET',])
def item_view(request, id, format=None):
    if request.method == 'GET':
        
        item = menu_item.objects.get(id=id)
        serializer = MenuItemSerializer(item)
        return Response(serializer.data)