/*************************Main*********************/
$(document).ready(function(){
    //check if an order is currently in progress. Go to the status page if yes
  if (getCurrentOrderId() !== null && getCurrentOrderId() !== undefined) {
    // move to status html page
    document.location.href = "status/?order_id=" + getCurrentOrderId();
  }

  $('#sendOrderButton').on('click',orderBasket);

  fillMenuItemsContainerWithCurrentOrder();
  updateViewWithCurrentLocalOrder();
 
  updateTotal();

  $('.adaptTextSize').textfill({
    maxFontPixels    : 20,
    //minFontPixels    : 4,
    //widthOnly : true,
    //debug : true,
  });
});



/**************************************************/

function updateTotal() {
    getLocalOrder().then (function(order){
      var currentTotal = order.totalPrice();
      document.getElementById("total").innerHTML = "Total : " + currentTotal.toFixed(2) + " €";
    });

}

function fillMenuItemsContainerWithCurrentOrder() {
  getLocalOrder().then (function(order){
    var orderedItems = order.items;
    var menuItems = order.menu.items;
  

    var items = {};
    for (var itemId in orderedItems) {
      items[itemId] = menuItems[itemId];
    }

    fillMenuItemsContainer(items);
  });
}