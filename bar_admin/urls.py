from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    url(r'^(?P<id>[0-9]+)/$', views.menu),
    url(r'^category/(?P<id>[0-9]+)/$', views.category),
    url(r'item/(?P<id>[0-9]+)/$', views.item_view),
]

urlpatterns = format_suffix_patterns(urlpatterns)
