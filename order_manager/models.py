from django.db import models
from django.utils import timezone

from django.conf import settings

class Order(models.Model):
    
    PENDING_STATUS = 'P'
    ACCEPTED_STATUS = 'A'
    REFUSED_STATUS = 'R'
    READY_STATUS = 'O'
    CANCELLED_STATUS = 'C'

    STATUS_CHOICES = (
        (PENDING_STATUS, 'Pending'),
        (ACCEPTED_STATUS, 'Accepted'),
        (REFUSED_STATUS, 'Refused'),
        (READY_STATUS, 'Ready'),
        (CANCELLED_STATUS, 'Cancelled')
    )
    
    comment = models.CharField(max_length=200, default="")
    pseudo = models.CharField(max_length=50, default="")
    
    status = models.CharField(max_length=1,
                                      choices=STATUS_CHOICES,
                                      default=PENDING_STATUS)
    requestedTime = models.DateTimeField(default=timezone.now)
    modifiedTime = models.DateTimeField(default=timezone.now)
    
    processedTime = models.DateTimeField(default=timezone.now) #once processed, the order should not be modified anymore
    
    client_user = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, blank = True, null=True)

    def __str__(self):
        #return str(self.id + " " + str(self.requestedTime))
        return str(self.id) + self.status
        

#TODO : change the class name :-)
class order_details(models.Model):
    
    name = models.CharField(max_length=200)
    quantity = models.IntegerField(default=0)
    order = models.ForeignKey(Order,null=True) #an order detail must be linked to an order
    def __str__(self):
        return self.name