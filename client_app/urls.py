from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^client/category/$', views.category_view),
    url(r'^client/my_order/status/$', views.my_order_status_view),
    url(r'^client/my_order/$', views.my_order_view),
    url(r'^client/$', views.hello),
    url(r'^$', views.home),
]
