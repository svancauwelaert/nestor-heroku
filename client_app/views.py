from django.shortcuts import render_to_response, RequestContext

from django.conf import settings
from django.views.decorators.csrf import ensure_csrf_cookie
from django.contrib.auth.decorators import login_required

from bar_admin.models import Menu

def home(request):
    return render_to_response("home.html", locals(), context_instance=RequestContext(request))

def hello(request):
    menus = Menu.objects.all()
    menu = menus.get(id=1)   #TODO : To be changed. For this proto we simply take the 1st menu
    
    if menu.available:
        return render_to_response("menu.html", locals(), context_instance=RequestContext(request))
    else:
        return render_to_response("menu-closed.html", locals(), 
                                  context_instance=RequestContext(request))

def category_view(request):
    menus = Menu.objects.all()
    menu = menus.get(id=1)
    
    cat_id = request.GET.get('cat_id', '1')
    categories = menu.categories.all()
    category = categories.get(id=cat_id)
    
    return render_to_response("category.html", locals(), context_instance=RequestContext(request))

@ensure_csrf_cookie
def my_order_view(request):
    menus = Menu.objects.all()
    menu = menus.get(id=1)
    
    return render_to_response("my_order.html", locals(), context_instance=RequestContext(request))

#@login_required
def my_order_status_view(request):
        
    order_id = request.GET.get('order_id', '1')
    
    return render_to_response("my_order_status.html", locals(), 
                              context_instance=RequestContext(request))
    