/*************************Main*********************/
//$(document).ready(function(){
    
    $('.adaptTextSize').textfill({
        maxFontPixels    : 20
    });
    
    var localMenu2;

    getLocalMenu().then (function(localMenu){
        localMenu2 = localMenu;
        setCategoryTitleView();
        
        fillMenuItemsContainerOfCurrentCategory();
        updateViewWithCurrentLocalOrder();
        //updateViewWithInfo();
    });
//});

/**************************************************/

function getCategoryFromURL() {
    var params={};window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(str,key,value){params[key] = value;});
    var categoryId = params['cat_id'];
    var category = localMenu2.categories[categoryId];
    return category;
} 

function fillMenuItemsContainerOfCurrentCategory() {
    var category = getCategoryFromURL();
    fillMenuItemsContainer(category.items);
}

function setCategoryTitleView() {
    var categoryName = getCategoryFromURL().name;
    $('#category-title').text(categoryName);
}
