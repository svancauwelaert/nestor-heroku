function acceptOrder(orderId) {
    "use strict";
    changeOrderStatus(orderId, 'A');
}

function refuseOrder(orderId) {
    "use strict";
    changeOrderStatus(orderId, 'R');
}

function setOrderReady(orderId) {
    "use strict";
    changeOrderStatus(orderId, 'O');
}

/*
 * orderId : id of an order that exists in the DB
 * newStatus : one of the status defined in the app order_manager/models.py
 */
function changeOrderStatus(orderId, newStatus) {
    "use strict";
    var jsonAckOrder = {id: orderId, status: newStatus};
    sendNewStatusChange(jsonAckOrder);
}

function sendNewStatusChange(jsonAckOrder) {
    "use strict";
    $.ajax({
        url: "/order_manager/change-order-status/", // the endpoint
        type: "POST", // http method
        data: jsonAckOrder, // data sent with the post request
        // handle a successful response
        success: function () {
            $('#post-text').val(''); // remove the value from the input
            location.reload();
        },

        // handle a non-successful response
        error: function (xhr, errmsg, err) {
                    $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: " + errmsg +
                    " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
        }
    });
}