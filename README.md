django security checklist + pony check up
============================================================

https://docs.djangoproject.com/fr/1.9/howto/deployment/checklist/
https://www.ponycheckup.com/

local postgres database
============================================================

export DATABASE_URL=postgres:///$(whoami)

In case of troubles when adding a field, try :

1. python manage.py makemigrations (possibly with the --fake option), **before** adding the field

2. python manage.py migrate

3. python manage.py makemigrations , **after** adding the field

4. python manage.py migrate

django static files in deployement
============================================================
python manage.py collectstatic

(see https://docs.djangoproject.com/en/1.8/howto/static-files/)

launch the server to be accessible in LAN
===================================================
* python manage.py runserver 0.0.0.0:8000
* serverIP = IP of the computer used to run the django server. (e.g., 192.168.1.3). On UNIX systems (linux, macOS), it can be found with the *ifconfig* command and on windows *ipconfig* 
* in the browser : http://serverIP:8000/*djangoApp* where *djangoApp* is the url of the desired app.

testing
=========

* launch site

     in cmd line go to mysite directory :
     $ python manage.py runserver

     in internet explorer go to 

	1. http://127.0.0.1:8000/admin/ = admin page (manage user and menu)

	1. http://127.0.0.1:8000/menu/1/?format=json   = get a menu as json 

	3. http://127.0.0.1:8000/order_manager/ = see current orders

* html communication

     in cmd line :
     $ python manage.py shell

```
    >>> from django.test import Client
    >>> c = Client()
    >>> response = c.get('/menu/json/')
    >>> response.status_code
    >>> response.content
```

* increment data_base

     in cmd line :
     $ python manage.py shell

```
    >>> from order_manager.models import order_details
    >>> q = order_details(name="hello",quantity=2.0)
    >>> q.save()
    >>> response.status_code
    >>> order_details.objects.all
```