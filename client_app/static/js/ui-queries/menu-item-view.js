function updateViewWithCurrentLocalOrder() {
    "use strict";
    getLocalOrder().then (function(order){
        var orderItems = order.items;
        for (var itemId in orderItems) {
            var item = orderItems[itemId];
            var input = document.getElementById("item-"+itemId+"-input");
            if(input) {
                input.innerHTML = item.quantity;
            }
        }
    });
}

function fillMenuItemsContainer(items) {
    for (var menuItemId in items) {
        var item = items[menuItemId];
        $('#menu-items-container').append(menuItemListHTML(item.name,item.id, item.price));
    }
    
    updateViewWithInfo(items);
}

//view of a menu item
function menuItemListHTML(name, id, price) {

	var html = '<div class="row " style="padding-top: 1%;  height: 10%; border: 1px solid #eee;" name=" ' + name +'"> \
        <div class="adaptTextSize col-xs-5" style="padding-left: 2%; min-height: 100% ;  height:1.3cm ; "> \
          <span class="short-div" id="item-' + id + '-name" style="text-align: left; ">' + name + '</span> \
        </div> \
        <div class="col-xs-2" style="  min-height: 100% ;  height:1.3cm ; font-size:x-small;"> \
            <div class="short-div glyphicon glyphicon-info-sign" style="height:0.8cm; text-align: right; display:block;" id="item-'+ id + '-info"></div> \
          <div id="item-' + id +'-price" style="text-align: right; vertical-align: bottom">' + price +'€</div> \
        </div> \
        <div class="col-xs-5 " style=" padding-left: 0; padding-right: 0;   height:1.3cm ;"> \
          <div class="input-group pull-right"> \
            <button type="button" class="btn btn-default btn-number" data-type="minus" data-field="quant[1]"  onclick="decrementOrder('+ id +');updateViewWithCurrentLocalOrder();updateHeaderTotal();"> \
                    <span class="glyphicon glyphicon-minus"></span> \
            </button> \
            <p id="item-' + id +'-input" style="display:inline-block; width:2em; text-align: center;">0 </p> \
            <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]" onclick="incrementOrder('+ id +');updateViewWithCurrentLocalOrder();updateHeaderTotal();"> \
                  <span class="glyphicon glyphicon-plus"></span> \
            </button> \
          </div> \
        </div> \
      </div>';

    return html;
}

function menuItemModalHTML(name, id, description) {
    var html =  
'<div id="modal-'+id+'-info" class="modal fade" role="dialog">  \
  <div class="modal-dialog">  \
    <!-- Modal content-->  \
    <div class="modal-content">  \
      <div class="modal-header">  \
        <button type="button" class="close" data-dismiss="modal">&times;</button>  \
        <h4 class=modal-title">'+name+'</h4>  \
      </div>  \
      <div class="modal-body">  \
        <p>'+description+'</p>  \
      </div>  \
      <div class="modal-footer">  \
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  \
      </div>  \
    </div>  \
  </div>  \
</div>';  
    
    return html;
}

function showItemInfo(item_id) {
  "use strict";
  getLocalMenu().then (function(localMenu){
    var item_tmp = localMenu.items[item_id];
    $('#modal-'+item_id+'-info').modal();
  });
}

// parse the index from the id string "attrPrefix12...
function getId(element, attrPrefix){
    var prefix = attrPrefix;
    var num = parseInt(element.attr("id").substring((prefix.length)));
    return num;
}

function updateViewWithInfo(items) {
    "use strict";
    $("[id ^=item-][id $=-info]")
    .each(function() {
        var item_id = getId($(this), "item-");
        //var items = localMenu2.items;
        if(items[item_id].description == "")  // item without description

        {
            //$(this).hide();
            $(this).removeClass('glyphicon-info-sign');
            $(this).innerHTML = " ";
        }
        else // item with description
        {
            $(this).css('cursor', 'pointer');
            $(this).click(function(){
                showItemInfo(item_id);
            });
            $('#menu-items-modal-container').append(menuItemModalHTML(items[item_id].name, item_id, items[item_id].description));
        }
    });

}