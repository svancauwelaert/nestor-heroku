function isLocalOrderAvailable() {
  return localStorage.localOrder !== undefined;
}

function updateLocalOrder(order) {
  localStorage.localOrder = JSON.stringify(order);
}



//ensures there is an order in session storage
function checkLocalOrderExistence() {
  
  // menu is a promise, order constructor needs a menu ==> order returned as a promise
  var OrderPromise = new jQuery.Deferred();
  
  if (!isLocalOrderAvailable()) {
    return getLocalMenu().then (function(localMenu){
      
      var order = new Order(localMenu);
      updateLocalOrder(order);
      return OrderPromise.resolve(order);
    })
  }
  else{
    return OrderPromise.resolve(deserialiseOrder(localStorage.localOrder));
  }
  
}

function getLocalOrder() {
    return checkLocalOrderExistence().then (function(order){
      return order; //deserialiseOrder(localStorage.localOrder);
  })
}

function incrementOrder(itemId) {
  "use strict";
  //checkLocalOrderExistence();
  getLocalOrder().then (function(order){
    order.incrementItem(itemId);
    updateLocalOrder(order);
  });
}

function decrementOrder(itemId) {
  "use strict";
  getLocalOrder().then (function(order){
    order.decrementItem(itemId);
    updateLocalOrder(order);
  });
}

function saveCurrentOrderId(newOrderId) {
  getLocalOrder().then (function(order){
    order.id = newOrderId;
    updateLocalOrder(order);
  });
}

function getCurrentOrderId() {
  getLocalOrder().then (function(order){
    return order.id;
  });
}

function setCurrentOrderStatus(newStatus) {
  getLocalOrder().then (function(order){
    order.status = newStatus;
    updateLocalOrder(order);
  });
}

function getCurrentOrderStatus() {
  return getLocalOrder().then (function(order){
    return order.status;
  });
}

//called when the order was either received by the client or refused by the bar tender and the client wants to order something else
function terminateOrder() {
  localStorage.clear();
  document.location.href = "/client/";
}

//have to add the custom prototype information when we deserialize the order
function deserialiseOrder(jsonOrder) {
  var order = new Order(getLocalMenu());
  $.extend(order, JSON.parse(jsonOrder));
  var items = order.items;
  for(var i in items) {
      var item = items[i];
      order.items[i] = new OrderItem(item.menuItemId, item.name, item.quantity);
  }
  return order;
}
