from django.conf.urls import patterns, url

from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    url(r'^$', views.home),
    url(r'^old-orders/$',views.oldProcessedOrders)
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)