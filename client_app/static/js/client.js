function sendBasket(jsonBasket, identifier, theComment) {
      $.ajax({
          url : "/order_manager/", // the endpoint
          type : "POST", // http method
          data : { json : JSON.stringify(jsonBasket), pseudo : identifier, comment : theComment}, // data sent with the post request
          // handle a successful response
          success : function(json) {
              //if(json.isUserAuthenticated) {
                saveCurrentOrderId(json.order_id);
                setCurrentOrderStatus(Order.pendingStatus);
                resetAllInput();

                // move to status html page
                document.location.href = "status/?order_id="+json.order_id;  
              /*}
              else {
                document.location.href = "/accounts/login/";  
              }*/
          },

          // handle a non-successful response
          error : function(xhr,errmsg,err) {
              alert("error ! ");
              $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                  " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
              alert(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
          }
      });

   function poll(orderId) {
    setTimeout(function() {
     $.ajax({
          url: "/order_manager/order_status/",
          type : "GET", // http method
          data : {id : orderId },
          success: function(data) {
              alert("status : " + data.order_status);
         },
          // handle a non-successful response
          error : function(xhr,errmsg,err) {
              $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                  " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
              alert("error ! "+ "error_msg : " + errmsg + xhr.status + ": " + xhr.responseText);
          },
         complete: poll(orderId)
       });
    }, 30000);
  }
}

function orderBasket() {
    var jsonBasket = [];

    getLocalOrder().then (function(order){
      var orderItems = order.items;

      for (var itemId in orderItems) {
          var item = orderItems[itemId];
          if(item.quantity > 0) {
            jsonBasket.push(
                          { 
                              "name" : item.name,
                              "quantity" : item.quantity
                          }
            );
          }
      }
      var identifier = $("#identifier-text").val();
      var comment = $("#comment-text").val();
      sendBasket(jsonBasket, identifier, comment);
    });
}