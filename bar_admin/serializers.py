
from rest_framework import serializers
from bar_admin.models import Menu, Menu_category, menu_item

class MenuItemSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = menu_item
        # fields that get serialized/deserialized
        fields = ('id','name','price','description')

        
class CategorySerializer(serializers.ModelSerializer):
    
    items = MenuItemSerializer(many=True, read_only=True)
    
    class Meta:
        model = Menu_category
        # fields that get serialized/deserialized
        fields = ('id','name','items')
    
    
class MenuSerializer(serializers.ModelSerializer):
    
    categories = CategorySerializer(many=True, read_only=True)
    
    class Meta:
        model = Menu
        # fields that get serialized/deserialized
        fields = ('id','available','categories')