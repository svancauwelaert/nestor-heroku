from django.contrib import admin

from .models import Order, order_details

class OrderAdmin(admin.ModelAdmin):
    fields = ['status','requestedTime','modifiedTime', 'processedTime', 'comment', 'pseudo', 'client_user']
    list_display = ('status','requestedTime','modifiedTime', 'processedTime', 'comment', 'pseudo', 'client_user')

admin.site.register(Order, OrderAdmin)

class order_detailsAdmin(admin.ModelAdmin):
    fields = ['name','quantity','order']
    list_display = ('name','quantity','order',)

admin.site.register(order_details, order_detailsAdmin)

