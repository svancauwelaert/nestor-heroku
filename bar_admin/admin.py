from django.contrib import admin

from .models import Menu, menu_item, Menu_category

class MenuItemInline(admin.TabularInline):
    model = menu_item
    
class MenuItemCategInline(admin.TabularInline):
    model = Menu_category


class MenuAdmin(admin.ModelAdmin):
    inlines = [
        MenuItemCategInline,
        MenuItemInline,
    ]
    fields = ['available']
    list_display = ('available',)

admin.site.register(Menu, MenuAdmin)

#class MenuItemAdmin(admin.ModelAdmin):
#    fields = ['name', 'price', 'menu']
#    list_display = ('name', 'price', 'menu')

#admin.site.register(menu_item, MenuItemAdmin)
