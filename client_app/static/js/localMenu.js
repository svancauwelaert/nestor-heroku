
function isLocalMenuAvailable() {
  return sessionStorage.localMenu !== undefined;
}

function downloadMenufromServer(menu_id) {
    return $.ajax({
          dataType: "json",
          url: "/menu/"+menu_id+".json",
          type : "GET", // http method
          success: function(data) {
              sessionStorage.localMenu = JSON.stringify(deserialiseMenu_server(JSON.stringify(data)));
         },
          // handle a non-successful response
          error : function(xhr,errmsg,err) {
              $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                  " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
              alert("error ! "+ "error_msg : " + errmsg + xhr.status + ": " + xhr.responseText);
          },
    });
}

function updateLocalMenu() {
  //TODO : download the menu from the server (if it changed). For now we don't check if it changed
  return downloadMenufromServer(1);  // for now we consider only first menu
}

//have to add the custom prototype information when we deserialize the menu
function deserialiseMenu_server(jsonMenu) {
  var menu = new Menu();
  var menu_tmp = new Menu();
  $.extend(menu_tmp, JSON.parse(jsonMenu));
  var categories = menu_tmp.categories;
  //for(var c in categories) {
  for(var c = 0; c < categories.length; c++) {
        var category = categories[c];
        menu.categories[category.id] = new MenuCategory(category.id, category.name, category.items);
        var categoryItems = category.items;
        for(var i in categoryItems) {
            var item = categoryItems[i];
            var prototypedItem = new MenuItem(item.id, category.id, item.name, item.price, item.description);
            menu.categories[category.id].items[item.id] = prototypedItem;
            menu.items[item.id] = prototypedItem;
        }
    }
    //alert("menu server " + JSON.stringify(menu.items));
  return menu;
}

function deserialiseMenu_storage(jsonMenu) {
  var menu = new Menu();
  $.extend(menu, JSON.parse(jsonMenu));
  var categories = menu.categories;
  for(var c in categories) {
  //alert(JSON.stringify(categories));
  //for(var c = 0; c < categories.length; c++) {
        var category = categories[c];
        menu.categories[category.id] = new MenuCategory(category.id, category.name, category.items);
        var categoryItems = category.items;
        for(var i in categoryItems) {
        //alert(JSON.stringify(categoryItems));
        //for(var i= 0; i < categoryItems.length; i++) {
            var item = categoryItems[i];
            //alert(JSON.stringify(item.categoryId));
            var prototypedItem = new MenuItem(item.id, item.categoryId, item.name, item.price, item.description);
            menu.categories[category.id].items[item.id] = prototypedItem;
            menu.items[item.id] = prototypedItem;
        }
    }
    //alert("menu storage " + JSON.stringify(menu.items));
  return menu;
}

// a cleaner way can be found in http://mattsnider.com/using-promises-to-cache-static-ajax-json-data/
// to do, if a download is already 'en cours', no need to make another ajax request
function getLocalMenu() {
    
    var localMenuPromise;
    // get a promise of localMenu (if not downloaded yet, download it, else retrieve it from session storage)
    if (!isLocalMenuAvailable()) {
        localMenuPromise = downloadMenufromServer(1);
    }
    else{
        localMenuPromise = new jQuery.Deferred();
        var local_menu = deserialiseMenu_storage(sessionStorage.localMenu);
        localMenuPromise.resolve(local_menu);  // resolved so the "then" event can be directly trigerred
    }
  
  return localMenuPromise.then(function(data){
         return deserialiseMenu_storage(sessionStorage.localMenu);
  });
}