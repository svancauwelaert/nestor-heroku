from django.db import models


class Menu(models.Model):
    available = models.BooleanField(default=True)
    def __str__(self):
        return str(self.id)
    
class Menu_category(models.Model):
    name = models.CharField(max_length=200)
    menu = models.ForeignKey(Menu,null=True, related_name='categories') #a menu category must be linked to menu
    def __unicode__(self):
        return unicode(self.name)
    def __str__(self):
        return str(self.name)

class menu_item(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=1000, default="", null=True, blank=True)
    price = models.FloatField(default=0.0)
    menu = models.ForeignKey(Menu,null=True) #a menu item must be linked to menu
    menu_cat = models.ForeignKey(Menu_category,null=True, related_name='items') #a menu item must be linked to menu category
    def __unicode__(self):
        return unicode(self.name)
    def __str__(self):
        return self.name
