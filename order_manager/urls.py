from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^change-order-status/$', views.changeOrderStatus),
    url(r'^order_status/$', views.orderStatus),
    url(r'^$', views.order, name='order'),
]
