
/***************Menu*****************************/

// class MenuItem
function MenuItem(id, categoryId, name, price, description) {
    "use strict";
    this.id = id;
    this.categoryId = categoryId;
    this.name = name;
    this.price = price;
    if (typeof description !== 'undefined') {
    	this.description = description;
    }
    else {
    	this.description = "";
    }
}

// class MenuCategory
function MenuCategory(id, name, items) {
	"use strict";
	this.id = id;
    this.name = name;
    this.items = {};
    //if (typeof items !== 'undefined') {
    //    for (var i = 0; i < items.length; i++) {
    //        var item = items[i];
    //        //to be able to access the items by their id
    //        this.items[item.id] = item;
    //    }
    //}

    this.nbItems = function () {return Object.keys(this.items).length;};
}

// class Menu
function Menu(categories) {
	"use strict";
    if (categories !== undefined) {
    	this.categories = categories;
    }
    else {
    	this.categories = {};
    }

    //to be able to access the items by their id
    this.items = {};
    //for(var c in categories) {
    //    var categoryItems = categories[c].items;
    //    for(var i in categoryItems) {
    //        var item = categoryItems[i];
    //        this.items[item.id] = item;
    //    }
    //}

    this.nbCategories = function () {return Object.keys(this.categories).length;};
}

/***************Order*****************************/
function OrderItem(menuItemId, name, quantity) {
    this.menuItemId = menuItemId;
    this.name = name;
    this.quantity = quantity;

    this.decrement = function() {
        if(this.quantity > 0) {
            this.quantity -= 1;        
        }
    };

    this.increment = function() {
        this.quantity += 1;        
    };
}

function Order(menu) {

    Order.pendingStatus = 'P';
    Order.acceptedStatus = 'A';
    Order.refusedStatus = 'R';
    Order.readyStatus = 'O';

    this.id = null;
    this.status = null;
    this.items = {};
    this.menu = menu;

    this.addItem = function(item) {
        this.items[item.menuItemId] = item;
    };

    this.incrementItem = function(itemId) {
        var itemInOrder = this.items[itemId];
        if(itemInOrder !== undefined) {
            itemInOrder.increment();
        }
        else{
            var itemName = this.menu.items[itemId].name;
            var item = new OrderItem(itemId, itemName, 1);
            this.addItem(item);
        }
    };

    this.decrementItem = function(itemId) {
        var itemInOrder = this.items[itemId];
        if(itemInOrder !== undefined) {
            itemInOrder.decrement();
        }
    };

    this.totalPrice = function() {
        var total = 0;
        var items = this.items;
        for(var i in items) {
            var item = items[i];
            total += this.menu.items[i].price * item.quantity;
        }
        return total;
    };

    this.numberOfItemsInCategory = function(categoryId) {
        var nbItems = 0;
        var items = this.items;

        for(var i in items) {
            var item = items[i];
            
            if(this.menu.items[i].categoryId === parseInt(categoryId)) {
                nbItems += item.quantity;    
            }
        }
        return nbItems;
    };

}
